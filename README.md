## Testo dell'esercizio

Implementare una web application laravel* che dopo il login di utente tramite username e password fornisca un token.

Non è necessario la fase di registrazione dell'utente con invio mail, puoi immaginare di partire con un db già contenente un utenza user: "root" password: "password"

Utilizzare il token recuperato dopo l'accesso per interrogare un api esposta dal sistema che faccia da proxy verso Punk API e mostri una lista paginata di birre.

Prestare particolare attenzione alla struttura del codice ed al riuso

E' gradita la presenza di test e l’utilizzo di docker



## Svolgimento

Se l'applicazione risponde al login con un token, deve trattarsi o di una SPA o di un webservice. Ho scelto la seconda ipotesi.

Coerentemente con questo scenario, ho ipotizzato che non servisse una view per il login e che il responso paginato dell'api fosse un json.



## Implementazione.

Il login prevede una route che risponde al post delle credenziali. Il relativo controller risponde direttamente con un json che contiene il token.

Il token viene generato dal modulo Sanctum di Laravel, più che sufficiente per gestire un caso semplice come questo, e può essere utilizzato  per chiamare l'endpoint `/api/mybeers` (protetto da Sanctum).

Il controller `MyBeerController.php` esegue la connessione all'ipotetica api di terze parti punkyApi tramite il client `\App\Http\ApiHelpers\Punky\Client` che in realtà legge dal DB della nostra stessa applicazione.

E' il client a ritornare i risultati già paginati perché mi pareva più verosimile che, in presenza di molti risultati, fosse l'api all'origine a compiere una prima paginazione.

L'oggetto di classe `LengthAwarePaginator`, quando ritornato da un controller genera automaticamente il json corrispondente.

La route 'beers' in `routes/web.php` è stata invece lasciata libera a puro scopo di ricontrollo.




## Utilizzo e tests

In assenza di views la maniera più semplice di verificare il funzionamento dell'app è attraverso i test.

Questa implementazione è praticamente tutto boiler-plate, quindi non c'era molto, in verità, da testare, ma ho aggiunto un feature test (che percorre e testa tutto il processo dal login all'ottenimento di due pagine di risultati) e un unit test che testa il client di punkyAPI.

Alternativamente, potete ovviamente utilizzare curl o strumenti tipo postman.




## Installazione

Questo esercizio è stato sviluppato su framework Laravel 8 con sail.
 
Per semplicità, ho lasciato tutti i container e tutte le password e le utenze di default, anche se questa applicazione utlizza solo MySQL e PHP8.

[Sail](https://laravel.com/docs/8.x/sail#configuring-a-bash-alias) è compreso nell'installazione di Laravel.

Prima di far girare l'applicazione è necessario far correre le migrazioni e i seed:

`sail artisan migrate`

`sail artisan db:seed`




## License

Questo esercizio è stato svolto da [David Ballerini](mailto:david.ballerini@gmail.com) con grave pregiudizio del pochissimo tempo libero che aveva a disposizione e molta nostalgia per il lavoro di sviluppatore
