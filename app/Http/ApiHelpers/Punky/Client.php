<?php


namespace App\Http\ApiHelpers\Punky;

use App\Models\Beer;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class Client is a very fake and very simple API client
 * which, in reality, reads records from our own DB and paginates them
 *
 * @package App\Http\ApiHelpers\Punky
 */
class Client
{
    /**
     * @param int $page
     * @return LengthAwarePaginator
     */
    public function get($page = 1)
    {
        return Beer::orderBy('id')->paginate(5, ['*'], 'page', $page);
    }
}
