<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     */
    public function authenticate(Request $request)
    {
        $resp = ['success' => 'no', 'token' => null];

        $credentials = $request->only('name', 'password');

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            $resp['success'] = 'ok';
            $resp['token'] = $request->user()->createToken('api', ['beers:see'])->plainTextToken;
        }

        return $resp;
    }
}
