<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\ApiHelpers\Punky\Client;

class MyBeerController extends Controller
{
    public function show(Request $request)
    {

        //
        // fake API Client
        //
        $client = new Client();

        //
        // fake API call to punkyAPI via a fake client
        //
        $resp = $client->get($request->query('page',1));
        // a real API call would handle exceptions here and/or non 200 response codes

        // results from paginations can be returned as json responses
        return $resp;
    }
}
