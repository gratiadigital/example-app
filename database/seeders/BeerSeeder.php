<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BeerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $beers = [];
        foreach (range(0,9) as $num)
        {
            /**
             * Beers are being created with a simple indexing for ease of debugging
             * (as that will make pagination very very clear)
             */
            $beers[] = ['brand' => "beer$num"];
        }


        DB::table('beers')->truncate();
        DB::table('beers')->insert($beers);
    }
}
