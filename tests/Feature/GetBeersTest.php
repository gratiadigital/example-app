<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetBeersTest extends TestCase
{
    /**
     * A basic feature test that walks the entire process up to getting a paginated api response.
     *
     * @return void
     */
    public function test_getBeers()
    {
        echo "Testing our intoxicating beer API...\n";


        //
        // check that the user exists
        //
        $root = User::where('name', 'root')->first();
        $this->assertNotNull($root);

        //
        // checks that it can be logged in
        //
        $response = $this->post(
            '/login',
            ['name' => 'root', 'password' => 'password']
        );
        $response->assertStatus(200);
        $this->assertAuthenticatedAs($root);

        //
        // Checks the token
        //
        $jsonResp = $response->decodeResponseJson();
        $this->assertEquals('ok', $jsonResp['success']);
        $this->assertArrayHasKey('token', $jsonResp);
        $this->assertNotEmpty($jsonResp['token']);

        //
        // gets paginated response (page 1)
        //
        echo "----- Page 1: -----\n";
        $token = $jsonResp['token'];
        $beers = $this->get(
            '/api/mybeers',
            ['Authorization' => $token]
        );
        $beers->assertStatus(200);

        $beersJson = $beers->decodeResponseJson();
        print_r($beersJson->json());

        $this->assertArrayHasKey('current_page', $beersJson);
        $this->assertEquals(1, $beersJson['current_page']);
        $this->assertArrayHasKey('data', $beersJson);
        $this->assertIsArray($beersJson['data']);
        $this->assertEquals(5, count($beersJson['data']));


        //
        // gets paginated response (page 2)
        //
        echo "----- Page 2: -----\n";
        $beers = $this->get(
            '/api/mybeers?page=2',
            ['Authorization' => $token]
        );
        $beers->assertStatus(200);
        $beersJson = $beers->decodeResponseJson();
        print_r($beersJson->json());

        $this->assertArrayHasKey('current_page', $beersJson);
        $this->assertEquals(2, $beersJson['current_page']);
        $this->assertArrayHasKey('data', $beersJson);
        $this->assertIsArray($beersJson['data']);
        $this->assertEquals(5, count($beersJson['data']));


    }
}
