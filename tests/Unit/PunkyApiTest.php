<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Http\ApiHelpers\Punky\Client;
use App\Models\Beer;

class PunkyApiTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_punkyApiClient()
    {
        //
        // fake api client
        //
        $client = new Client();

        //
        // test for page 1
        //
        $p = $client->get();
        $beers = $p->items();

        $this->assertEquals(10, $p->total());
        $this->assertEquals(1, $p->currentPage());
        $this->assertCount(5, $beers);
        foreach(range(0,4) as $num)
        {
            $this->assertEquals("beer$num", $beers[$num]->brand);
        }

        //
        // test for page 2
        //
        $p = $client->get(2);
        $beers = $p->items();

        $this->assertEquals(10, $p->total());
        $this->assertEquals(2, $p->currentPage());
        $this->assertCount(5, $beers);
        foreach(range(5,9) as $num)
        {
            $this->assertEquals("beer$num", $beers[$num-5]->brand);
        }

    }
}
